import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/product';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-get-product-by-id',
  templateUrl: './get-product-by-id.component.html',
  styleUrls: ['./get-product-by-id.component.css']
})
export class GetProductByIdComponent implements OnInit {
  private sub: any;
  id:any;
  data: any = {};
  product = new Product();
  username:any;
  constructor(private route:ActivatedRoute, private dataService:DataService,public router: Router) { }
  alert:boolean = false;
  asset:any;
  ngOnInit(): void {
    console.log(this.route.snapshot.params.id);
    this.id = this.route.snapshot.params.id;
    this.getData();
    this.username = localStorage.getItem('username');
    this.asset = "http://localhost:8000/uploads/products/";
  }

  getData(){
    this.dataService.getProductById(this.id).subscribe(res =>{
      //console.log(res);
      this.data = res;
      //console.log(this.data[0].id);
      this.product = this.data[0];
    });
  }


  logout(){
    console.log('logged out');
    localStorage.removeItem('myToken');
    this.alert = true;

      setTimeout(() => {
        this.router.navigate(['user/login']);
     }, 3000);
   
  }

  closeAlert(){
    this.alert = false;
  }

}
