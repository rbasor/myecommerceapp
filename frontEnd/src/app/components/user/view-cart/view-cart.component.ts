import { Component, OnInit } from '@angular/core';
import { Cart } from 'src/app/cart';
import { Order } from 'src/app/order';
import { Product } from 'src/app/product';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-view-cart',
  templateUrl: './view-cart.component.html',
  styleUrls: ['./view-cart.component.css']
})
export class ViewCartComponent implements OnInit {
  [x: string]: any;
  index = 1;
  constructor(private dataService:DataService) { }
  cart:any;
  alert:boolean = false;
  alert2:boolean = false;
  order = new Order();
  product = new Product();
  myCart = new Cart();
  temp:any;
  asset:any;
  pq:any;
  ngOnInit(): void {
    this.getCartList();
    this.asset = "http://localhost:8000/uploads/products/";
  }

  getCartList(){
    this.dataService.getCartItems(localStorage.getItem('user_id')).subscribe( res=>{
      console.log(res);
      this.cart = res;
    });
  }

  cartToPlaceOrder(){
    this.dataService.cartToSave(localStorage.getItem('user_id'),this.order).subscribe(res =>{
     console.log(res);
     this.alert2 = true;
     
    });
  }


  closeAlert(){
    this.alert = false;
    this.alert2 = false;
  }


  deleteData(id: any){
    console.log(id);
    this.dataService.deleteFromCart(id).subscribe(res =>{
      this.getCartList();
      this.alert = true;
    });
  }

  increaseQuantity(cartId: any){

    this.dataService.showCart(cartId).subscribe(res =>{
       this.temp = res;
       this.myCart = this.temp[0];
      });
      this.myCart.quantity = this.myCart.quantity+1;
      this.dataService.upadateQuantity(cartId,this.myCart).subscribe(res =>{
       console.log(res);
       this.getCartList();
      });

   
  }

  decreaseQuantity(cartId: any){
    
     this.dataService.showCart(cartId).subscribe(res =>{
      this.temp = res;
      this.myCart = this.temp[0];
     });
     this.myCart.quantity = this.myCart.quantity-1;
     this.dataService.upadateQuantity(cartId,this.myCart).subscribe(res =>{
      console.log(res);
      this.getCartList();
     });

  }

}
