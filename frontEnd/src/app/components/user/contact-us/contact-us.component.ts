import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from 'src/app/customer';
import { Message } from 'src/app/message';

import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  alert:boolean = false;
  message = new Message();
  customer = new Customer();
  data:any;
  constructor(private dataService:DataService,public router: Router,private http:HttpClient) { }

  ngOnInit(): void {
    this.getData();
  }

  onSubmit() {
    this.dataService.insertMessage(this.message).subscribe(res =>{
      console.log(res);
      this.alert = true;

      setTimeout(() => {
        this.router.navigate(['user/getProduct']);
     }, 3000);
    });
  }

  getData(){
    this.dataService.getUserInfo().subscribe( res=>{
      this.data = res;
      this.customer = this.data['success'];
      this.message.name = this.customer.name;
      this.message.user_id = this.customer.id;
      this.message.email = this.customer.email;
    });
  }


  closeAlert(){
    this.alert = false;
  }

}
