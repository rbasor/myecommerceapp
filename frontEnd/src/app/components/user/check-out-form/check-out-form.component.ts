import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/customer';
import { Order } from 'src/app/order';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-check-out-form',
  templateUrl: './check-out-form.component.html',
  styleUrls: ['./check-out-form.component.css']
})
export class CheckOutFormComponent implements OnInit {
  len:any;
  constructor(private dataService:DataService) { }
  carts:any;
  noOfCartItem:any;
  customer = new Customer();
  order = new Order();
  ngOnInit(): void {
    this.getCartList();
    this.countCartItems();
  }

  getCartList(){
    this.dataService.getCartItems(localStorage.getItem('user_id')).subscribe( res=>{
      console.log(res);
      this.carts = res;
    });
  }

  countCartItems(){
    this.dataService.countCartItems(localStorage.getItem('user_id')).subscribe( res=>{
      console.log(res);
      this.noOfCartItem = res;
    });
  }


  cartToPlaceOrder(){
    this.dataService.cartToSave(localStorage.getItem('user_id'),this.order).subscribe(res =>{
     console.log(res);
     
    });
  }

}
