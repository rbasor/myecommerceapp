import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-track-order',
  templateUrl: './track-order.component.html',
  styleUrls: ['./track-order.component.css']
})
export class TrackOrderComponent implements OnInit {
  username:any;
  constructor() { }

  ngOnInit(): void {
    this.username = localStorage.getItem('username');
  }

}
