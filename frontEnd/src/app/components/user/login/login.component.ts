import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Customer } from 'src/app/customer';
import { DataService } from 'src/app/service/data.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  customer = new Customer();
  data: any = {};
 

  constructor(private dataService: DataService,public router: Router) { }

  ngOnInit(): void {
   
  }


  getUserInfo(){
    this.dataService.getUserInfo().subscribe(res =>{
     // console.log(res);
      
      this.data = res;
    //console.log(this.data['success'].name);
     // console.log(this.data.id);
      
      localStorage.setItem('user_id',  this.data['success'].id);
      localStorage.setItem('username',  this.data['success'].name);
      console.log("From localStorage user_id: " + localStorage.getItem('username') );
   
    });
  }

  loginUser(){
    console.log(this.customer);
    this.dataService.loginUser(this.customer).subscribe(res =>{
      //console.log(res['token']);
      this.getUserInfo();
      localStorage.setItem('myToken',  res['token']);
      localStorage.setItem('username',  res['name']);
      this.router.navigate(['user/getProduct']);
     // console.log("From localStorage: " + localStorage.getItem('myToken') + " "+ localStorage.getItem('username'));
    });
  }


  logout(){
    this.dataService.logout().subscribe(res =>{
      console.log(res);
    });
  }

}
