import { Component, OnInit } from '@angular/core';



import { HttpClient } from '@angular/common/http';

import { FormGroup, FormControl, Validators} from '@angular/forms';
import { DataService } from 'src/app/service/data.service';
import { Product } from 'src/app/product';

@Component({
  selector: 'app-insert-product',
  templateUrl: './insert-product.component.html',
  styleUrls: ['./insert-product.component.css']
})
export class InsertProductComponent implements OnInit {

  imageSrc!: string;
  product = new Product()
  myForm = new FormGroup({
   name: new FormControl('', [Validators.required, Validators.minLength(3)]),
   file: new FormControl('', [Validators.required]),
   category: new FormControl('', [Validators.required]),
   price: new FormControl('', [Validators.required]),
   specifications: new FormControl('', [Validators.required]),
   fileSource: new FormControl('', [Validators.required])
 });

  constructor(private http: HttpClient,private dataService: DataService) { }

  ngOnInit(): void {
  }

  onFileChange(event) {
    const reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
        this.myForm.patchValue({
          fileSource: reader.result
        });
      };
    }
  }


  submit(){
    //console.log(this.myForm.value);
    //this.dataService.insertObject(this.myForm.value).subscribe(res =>{
      //console.log(res);
      this.product.name = this.myForm.value.name;
      this.product.price = this.myForm.value.price;
      this.product.specifications = this.myForm.value.specifications;
      this.product.category = this.myForm.value.category;
      this.product.image = this.myForm.value.file;
    /*this.dataService.insertObject(this.product).subscribe(res =>{
        console.log(res);
      });*/
      console.log(this.myForm.value.file);
     // console.log(this.product);
  }


}
