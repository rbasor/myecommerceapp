import { Component, OnInit } from '@angular/core';
import {Customer } from 'src/app/customer';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  customer = new Customer();
  constructor(private dataService: DataService) { }

  ngOnInit(): void {
  }

  createUser(){
    console.log(this.customer);
    this.dataService.createUser(this.customer).subscribe(res =>{
      console.log(res);
    });
  }

}
