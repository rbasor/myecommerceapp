import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cart } from 'src/app/cart';
import { Order } from 'src/app/order';
import { Product } from 'src/app/product';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.css']
})
export class OrderPageComponent implements OnInit {
  alert:boolean = false;
  alert2:boolean = false;
  id:any;
  data: any = {};
  product = new Product();
  order = new Order();
  cart = new Cart();
  username:any;
  object:any;
  tmp:any = {};
  asset:any;
  constructor(private route:ActivatedRoute, private dataService:DataService,public router: Router) { }

  ngOnInit(): void {
   console.log(this.route.snapshot.params.id);
    this.id = this.route.snapshot.params.id;
    this.getData();
    this.username = localStorage.getItem('username');
    this.asset = "http://localhost:8000/uploads/products/";
  }

  getData(){
    this.dataService.getProductById(this.id).subscribe(res =>{
      //console.log(res);
      this.data = res;
      //console.log(this.data[0].id);
      this.object = this.data[0];
      this.order = this.data[0];
      this.cart = this.data[0];
      this.cart.image = this.data[0].image;
      this.order.product_id = this.data[0].id;
      this.order.name = this.data[0].name;
      this.order.user_id = localStorage.getItem('user_id');
    });
  }

  placeOrder(){
    
    this.order.quantity = 1;

    console.log(this.order);
   this.dataService.insertOrder(this.order).subscribe(res =>{
      console.log(res);
      this.alert = true;

      setTimeout(() => {
        this.router.navigate(['user/getProduct']);
     }, 3000);
     // this.router.navigate(['user/getProduct']);
    });
  }


  addToCart(pid){

    this.dataService.getProductById(pid).subscribe(res =>{
      console.log(res);
      this.tmp = res;
      //this.cart.image = this.tmp[0].image;
    this.cart = this.tmp[0];
    });

    //this.cart.product_id = pid;
   // this.order.user_id = localStorage.getItem('user_id');
    this.cart.quantity = 1;
   
    console.log(this.cart);
    this.dataService.insertCart(this.cart).subscribe(res =>{
      console.log(res);
      this.alert2 = true;

      setTimeout(() => {
        this.router.navigate(['user/getProduct']);
     }, 3000);
    });
  }

  logout(){
    console.log('logged out');
    localStorage.removeItem('myToken');
    this.alert = true;

      setTimeout(() => {
        this.router.navigate(['user/login']);
     }, 3000);
   
  }


  closeAlert(){
    this.alert = false;
  }
}
