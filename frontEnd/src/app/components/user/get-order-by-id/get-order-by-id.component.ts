import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Order } from 'src/app/order';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-get-order-by-id',
  templateUrl: './get-order-by-id.component.html',
  styleUrls: ['./get-order-by-id.component.css']
})
export class GetOrderByIdComponent implements OnInit {
  username:any;
  order:any;
  product:any;

  category:any;
  alert:boolean = false;
  asset:any;
  constructor(private dataService:DataService,public router: Router) { }

  ngOnInit(): void {
    this.username = localStorage.getItem('username');
    this.getOrderList();
    this.asset = "http://localhost:8000/uploads/products/";
  }

  getOrderList(){
    this.dataService.getOrderById(localStorage.getItem('user_id')).subscribe( res=>{
      console.log(res);
      this.order = res;
    });
  }

  deleteData(id){
    this.dataService.deleteOrder(id).subscribe(res =>{
      this.getOrderList();
    });
  }

  logout(){
    console.log('logged out');
    localStorage.removeItem('myToken');
    this.alert = true;

      setTimeout(() => {
        this.router.navigate(['user/login']);
     }, 3000);
   
  }

  closeAlert(){
    this.alert = false;
  }

}
