import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  username:any;
  constructor(private dataService:DataService) { }
  myNotifications:any;
  ngOnInit(): void {
    this.getNotifications();
    this.username  = localStorage.getItem('username');
  }

  getNotifications(){
    this.dataService.getNotifications(localStorage.getItem('user_id')).subscribe(res =>{
      console.log(res);
      this.myNotifications = res;
    });
  }

  readOrNot(mid){
    this.dataService.readOrNot(mid).subscribe(res =>{
      console.log(res);
    });
  }

}
