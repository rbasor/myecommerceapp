import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-get-all-product',
  templateUrl: './get-all-product.component.html',
  styleUrls: ['./get-all-product.component.css']
})
export class GetAllProductComponent implements OnInit {

  product:any;
  username: any;
  category:any;
  alert:boolean = false;
  constructor(private dataService:DataService,public router: Router) { }
  asset:any;
  noOfCartItem:any;
  noOfNotifications:any;
  tmp:any;
  ngOnInit(): void {
    this.getAll();
    this.username = localStorage.getItem('username');
   // console.log(this.username);
   this.asset = "http://localhost:8000/uploads/products/";
   this.countCartItems();
   this.countNotifications();
  this.tmp = "welcome.jpg";
  }

  getAll(){
    //console.log('RajTheGreat');
    this.dataService.getAllProduct().subscribe( res=>{
      console.log(res);
      this.product = res;
     // this.category = 'laptop';
    });
  }


  


  countCartItems(){
    this.dataService.countCartItems(localStorage.getItem('user_id')).subscribe( res=>{
      console.log(res);
      this.noOfCartItem = res;
    });
  }

  countNotifications(){
    this.dataService.countMyNotification(localStorage.getItem('user_id')).subscribe( res=>{
      console.log(res);
      this.noOfNotifications = res;
    });
  }


  setCategory(type: any){
    this.category = type;
  }

  logout(){
    console.log('logged out');
    localStorage.removeItem('myToken');
    this.alert = true;

      setTimeout(() => {
        this.router.navigate(['user/login']);
     }, 3000);
   
  }

  closeAlert(){
    this.alert = false;
  }

}
