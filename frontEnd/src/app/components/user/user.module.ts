import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { GetAllProductComponent } from './get-all-product/get-all-product.component';
import { GetOrderByIdComponent } from './get-order-by-id/get-order-by-id.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { ViewProductByIdComponent } from './view-product-by-id/view-product-by-id.component';
import { OrderPageComponent } from './order-page/order-page.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { RouterModule } from '@angular/router';
import { GetProductByIdComponent } from './get-product-by-id/get-product-by-id.component';
import { FormsModule } from '@angular/forms';
import {UserComponent } from './user.component';
import { TrackOrderComponent } from './track-order/track-order.component';
import { UserAddressComponent } from './user-address/user-address.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ViewCartComponent } from './view-cart/view-cart.component';
import { InsertProductComponent } from './insert-product/insert-product.component';
import { BrowserModule } from '@angular/platform-browser';


import { HttpClientModule } from '@angular/common/http';

import {  ReactiveFormsModule } from '@angular/forms';
import { CheckOutFormComponent } from './check-out-form/check-out-form.component';
import { NotificationsComponent } from './notifications/notifications.component';
const userRoute = [
  {path:'user', 
  children:[
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    {path:'login',component:LoginComponent},
    {path:'register',component:RegisterComponent},
    {path:'getProduct',component:GetAllProductComponent},
     /* children:[
          {path:'',component:GetAllProductComponent},
          {path:'getProductById/:id',component:GetProductByIdComponent},   
        ]},*/
    {path:'getProductById/:id',component:GetProductByIdComponent},  
   {path:'orderPage',component:OrderPageComponent},
    {path:'getOrderList',component:GetOrderByIdComponent},
    {path:'trackOrder',component:TrackOrderComponent},
    {path:'contactUs',component:ContactUsComponent},
    {path:'profile',component:UpdateProfileComponent},
    {path:'myCart',component:ViewCartComponent},
    {path:'insertProduct',component:InsertProductComponent},
    {path:'check-out',component:CheckOutFormComponent},
    {path:'notifications',component:NotificationsComponent},
  ]},
  {path:'orderPage/:id',component:OrderPageComponent},
 
]

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    GetAllProductComponent,
    GetOrderByIdComponent,
    UserHomeComponent,
    ViewProductByIdComponent,
    OrderPageComponent,
    UpdateProfileComponent,
    UserComponent,
    TrackOrderComponent,
    UserAddressComponent,
    ContactUsComponent,
    ViewCartComponent,
    InsertProductComponent,
    CheckOutFormComponent,
    NotificationsComponent
  ],
  imports: [
    BrowserModule,

    HttpClientModule,

    FormsModule,

    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(userRoute)
  ],
  bootstrap: [UserComponent]
})
export class UserModule { }
