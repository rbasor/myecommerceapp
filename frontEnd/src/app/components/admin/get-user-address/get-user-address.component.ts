import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-get-user-address',
  templateUrl: './get-user-address.component.html',
  styleUrls: ['./get-user-address.component.css']
})
export class GetUserAddressComponent implements OnInit {
  address :any;
  constructor(private dataService:DataService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.dataService.getAllAddress().subscribe( res=>{
      this.address = res;
    });
  }

}
