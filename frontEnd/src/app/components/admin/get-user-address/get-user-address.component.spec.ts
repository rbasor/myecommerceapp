import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetUserAddressComponent } from './get-user-address.component';

describe('GetUserAddressComponent', () => {
  let component: GetUserAddressComponent;
  let fixture: ComponentFixture<GetUserAddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetUserAddressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetUserAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
