import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { DataService } from 'src/app/service/data.service';
@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {
  data:any;
  constructor(private dataService:DataService) { }

  ngOnInit(): void {
      //Toggle Click Function
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });

      this.getData();
  }

  getData(){
    this.dataService.getAdminAllData().subscribe( res=>{
      console.log(res);
      this.data = res[0];
      console.log(this.data[0]);
    });
  }

}
