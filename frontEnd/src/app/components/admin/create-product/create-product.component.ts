import { Component, OnInit } from '@angular/core';

import { HttpClient ,HttpHeaders} from '@angular/common/http';

import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { DataService } from 'src/app/service/data.service';
import { Product } from 'src/app/product';
@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  product = new Product()
 

  constructor(private http: HttpClient,private dataService: DataService ,private formBuilder: FormBuilder) { }
  selectedFile: File;
  ngOnInit(): void {
    
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);

  }

  submit(data){
    const fd = new FormData();
    fd.append('name' , data.name);
    fd.append('price' , data.price);
    fd.append('specifications' , data.specifications);
    fd.append('category' , data.category);
    fd.append('image' , this.selectedFile);
    this.dataService.insertObject(fd).subscribe(res =>{
      console.log(res);
    });
    console.log(fd.getAll('name'));

  }

}
