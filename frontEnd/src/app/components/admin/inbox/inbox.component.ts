import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {
  message:any;
  constructor(private dataService:DataService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.dataService.getAllMessage().subscribe( res=>{
      this.message = res;
    });
  }

}
