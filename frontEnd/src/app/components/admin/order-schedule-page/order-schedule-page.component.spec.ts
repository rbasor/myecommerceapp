import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSchedulePageComponent } from './order-schedule-page.component';

describe('OrderSchedulePageComponent', () => {
  let component: OrderSchedulePageComponent;
  let fixture: ComponentFixture<OrderSchedulePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderSchedulePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSchedulePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
