import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'src/app/order';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-order-schedule-page',
  templateUrl: './order-schedule-page.component.html',
  styleUrls: ['./order-schedule-page.component.css']
})
export class OrderSchedulePageComponent implements OnInit {
  alert:boolean = false;
  order = new Order();
  id:any;
  data: any = {};
  asset:any;
  constructor(private dataService:DataService,public router: Router,private route:ActivatedRoute) { }

  ngOnInit(): void {
    console.log(this.route.snapshot.params.id);
    this.id = this.route.snapshot.params.id;
    this.getData();
    this.asset = "http://localhost:8000/uploads/products/";
  }


  onSubmit() {
    this.dataService.updateOrder(this.id,this.order).subscribe(res =>{
      console.log(res);

      this.alert = true;

      setTimeout(() => {
        this.router.navigate(['admin']);
     }, 3000);
     });
  }

  getData(){
    this.dataService.getOrder(this.id).subscribe(res =>{
      console.log(res);
      this.data = res;
     // console.log(this.data[0].id);
      //console.log(this.data[0].price);
     // console.log(this.data[0].category);
      this.order = this.data[0]
      this.order.price = this.data[0].price;
      this.order.status = this.data[0].status;
      this.order.category = this.data[0].category;
      console.log(this.order);
    });
  }


  closeAlert(){
    this.alert = false;
  }

}
