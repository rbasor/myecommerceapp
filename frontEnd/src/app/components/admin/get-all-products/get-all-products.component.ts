import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
@Component({
  selector: 'app-get-all-products',
  templateUrl: './get-all-products.component.html',
  styleUrls: ['./get-all-products.component.css']
})
export class GetAllProductsComponent implements OnInit {

  product:any;
  asset:any;
  constructor(private dataService:DataService) { }

  ngOnInit(): void {
    this.getAll();
    this.asset = "http://localhost:8000/uploads/products/";
  }

  getAll(){
    console.log('RajTheGreat');
    this.dataService.getAllProduct().subscribe( res=>{
      console.log(localStorage.getItem('name'));
      this.product = res;
    });
  }

  deleteData(id){
    console.log(id);
    this.dataService.deleteProduct(id).subscribe(res =>{
      this.getAll();
    });
  }

}
