import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-get-all-orders',
  templateUrl: './get-all-orders.component.html',
  styleUrls: ['./get-all-orders.component.css']
})
export class GetAllOrdersComponent implements OnInit {
  asset:any;
  order:any;
  constructor(private dataService:DataService,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.getAll();
    this.asset = "http://localhost:8000/uploads/products/";
  }

  getAll(){
    console.log('RajTheGreat');
    this.dataService.getAllOrder().subscribe( res=>{
      this.order = res;
      console.log(res);
    });
  }

  deleteData(id){
    console.log(id);
    this.dataService.deleteOrderByAdmin(id).subscribe(res =>{
      this.getAll();
    });
  }

}
