import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-get-all-users',
  templateUrl: './get-all-users.component.html',
  styleUrls: ['./get-all-users.component.css']
})
export class GetAllUsersComponent implements OnInit {
  user:any;
  constructor(private dataService:DataService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.dataService.getAllUser().subscribe( res=>{
      this.user = res;
    });
  }

}
