import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AddProductComponent } from './add-product/add-product.component';
import { GetAllProductsComponent } from './get-all-products/get-all-products.component';
import { GetAllOrdersComponent } from './get-all-orders/get-all-orders.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetUserAddressComponent } from './get-user-address/get-user-address.component';
import { GetAllUsersComponent } from './get-all-users/get-all-users.component';
import { InboxComponent } from './inbox/inbox.component';
import { OrderSchedulePageComponent } from './order-schedule-page/order-schedule-page.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateProductComponent } from './create-product/create-product.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
const adminRoute = [
  {path:'admin', 
  children:[
    {path:'',component:AdminHomeComponent},
    {path:'login',component:LoginComponent},
    {path:'inbox',component:InboxComponent},
    {path:'register',component:RegisterComponent},
    {path:'getProduct',children:[
      {path:'',component:GetAllProductsComponent},
      {path:'updateProduct/:id',component:UpdateProductComponent},
    
    ]},
    {path:'addProduct',component:CreateProductComponent},
    {path:'createProduct',component:CreateProductComponent},
    {path:'userAddress',component:GetUserAddressComponent},
    {path:'getUsers',component:GetAllUsersComponent},
    {path:'schedule/:id',component:OrderSchedulePageComponent},
    {path:'dashboard',component:DashboardComponent},
    {path:'getOrders',children:[
      {path:'',component:GetAllOrdersComponent},
    
    ]},
  ]},
]

@NgModule({
  declarations: [
    AdminHomeComponent,
    AddProductComponent,
    GetAllProductsComponent,
    GetAllOrdersComponent,
    LoginComponent,
    RegisterComponent,
    UpdateProductComponent,
    UpdateUserComponent,
    GetUserAddressComponent,
    GetAllUsersComponent,
    InboxComponent,
    OrderSchedulePageComponent,
    DashboardComponent,
    CreateProductComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(adminRoute)
  ]
})
export class AdminModule { }
