import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { Product } from 'src/app/product';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
 
  product = new Product();
  constructor(private dataService: DataService,public router: Router,private http:HttpClient) { }

  ngOnInit(): void {
  }

  

  insertProduct(){
    console.log(this.product);
    this.dataService.insertProduct(this.product).subscribe(res =>{
      console.log(res);
      this.router.navigate(['admin']);
    });
  }

}
