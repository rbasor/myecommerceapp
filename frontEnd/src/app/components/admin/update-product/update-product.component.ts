import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/product';
import { DataService } from 'src/app/service/data.service';
@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
  id:any;
  data: any = {};
  product = new Product();
  asset:any;
  object:any;
  constructor(private route:ActivatedRoute, private dataService:DataService,public router: Router) { }

  ngOnInit(): void {
    console.log(this.route.snapshot.params.id);
    this.id = this.route.snapshot.params.id;
    this.getData();
    this.asset = "http://localhost:8000/uploads/products/";
  }

  getData(){
    this.dataService.getProductById(this.id).subscribe(res =>{
      //console.log(res);
      this.data = res;
      //console.log(this.data[0].id);
      this.product = this.data[0];
      this.object = this.data[0];
    });
  }

  updateProduct(){
    this.dataService.updateProduct(this.id,this.product).subscribe(res =>{
     console.log(res);
     this.router.navigate(['admin/getProduct']);
    });
  }

}
