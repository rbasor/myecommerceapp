import { Component, OnInit } from '@angular/core';
import {Customer } from 'src/app/customer';
import { DataService } from 'src/app/service/data.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  customer = new Customer();
  constructor(private dataService: DataService) { }

  ngOnInit(): void {
  }

  loginUser(){
    console.log(this.customer);
    this.dataService.loginUser(this.customer).subscribe(res =>{
      //console.log(res['token']);
      localStorage.setItem('myToken',  res['token']);
      localStorage.setItem('username',  res['name']);
      console.log("From localStorage: " + localStorage.getItem('myToken') + " "+ localStorage.getItem('username'));
    });
  }


  logout(){
    this.dataService.logout().subscribe(res =>{
      console.log(res);
    });
  }

}
