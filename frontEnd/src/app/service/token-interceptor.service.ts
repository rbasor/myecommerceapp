import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { Content } from '@angular/compiler/src/render3/r3_ast';
@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req , next){
    let tokenizedReq = req.clone({
      setHeaders:{
        //'Content-Type': 'application/json',
        Authorization: 'Bearer '+localStorage.getItem('myToken'),
       // 'Content-Type': 'multipart/form-data'
      }

      
    });

    return next.handle(tokenizedReq)

  }
}
