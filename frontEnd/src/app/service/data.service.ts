import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cart } from '../cart';
import { Customer } from '../customer';

import { Message } from '../message';
import { Order } from '../order';
import { Product } from '../product';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient:HttpClient) { }


  createUser(data: Customer){
    return this.httpClient.post('http://127.0.0.1:8000/api/register/', data);
  }

  loginUser(data: Customer){
    return this.httpClient.post('http://127.0.0.1:8000/api/login/', data);
  }

  insertProduct(data: Product){
    return this.httpClient.post('http://127.0.0.1:8000/api/addProduct/', data);
  }

  insertMessage(data: Message){
    return this.httpClient.post('http://127.0.0.1:8000/api/addMessage/', data);
  }

  getAllProduct(){
    return this.httpClient.get('http://127.0.0.1:8000/api/getProduct');
  }

  getAllUser(){
    return this.httpClient.get('http://127.0.0.1:8000/api/getAllUsers');
  }

  deleteProduct(id: string){
    return this.httpClient.delete('http://127.0.0.1:8000/api/deleteProduct/'+id);
  }

  deleteUser(id: string){
    return this.httpClient.delete('http://127.0.0.1:8000/api/deleteUser/'+id);
 
  }

  updateProduct(id: string,data: Product){
    return this.httpClient.post('http://127.0.0.1:8000/api/updateProduct/'+id, data);
  }

  getProductById(id: string){
    return this.httpClient.get('http://127.0.0.1:8000/api/getProductById/'+id);
  }

  logout(){
    return this.httpClient.get('http://127.0.0.1:8000/api/logout');
  }

  uploadImage(id: string,data: any){
    return this.httpClient.post('http://127.0.0.1:8000/api/imageUpload/'+id, data);
  }

  getAllOrder(){
    return this.httpClient.get('http://127.0.0.1:8000/api/getAllOrder');
  }

  insertOrder(data: Order){
    return this.httpClient.post('http://127.0.0.1:8000/api/addOrder/', data);
  }

  insertCart(data: Cart){
    return this.httpClient.post('http://127.0.0.1:8000/api/addToCart/', data);
  }

  getCartItems(id: string | null){
    return this.httpClient.get('http://127.0.0.1:8000/api/viewCart/'+id);
  }

  countCartItems(id: string | null){
    return this.httpClient.get('http://127.0.0.1:8000/api/countCartItems/'+id);
  }



  getOrderById(id: string | null){
    return this.httpClient.get('http://127.0.0.1:8000/api/getOrderById/'+id);
  }

  getUserInfo(){
    return this.httpClient.get('http://127.0.0.1:8000/api/details');
  }

  deleteOrder(id: string){
    return this.httpClient.delete('http://127.0.0.1:8000/api/deleteOrder/'+id);
  }

  getAllAddress(){
    return this.httpClient.get('http://127.0.0.1:8000/api/getAllAddress');
  }

  getAddressByUserId(id: string){
    return this.httpClient.get('http://127.0.0.1:8000/api/getAddressByUserId/'+id);
  }

  

  cartToSave(id: string | null,data: Order){
    return this.httpClient.post('http://127.0.0.1:8000/api/cartToSave/'+id, data);
  }

  getAllMessage(){
    return this.httpClient.get('http://127.0.0.1:8000/api/getAllMessages');
  }

  getOrder(id: string){
    return this.httpClient.get('http://127.0.0.1:8000/api/getOrder/'+id);
  }

  updateOrder(id: string,data: Order){
    return this.httpClient.post('http://127.0.0.1:8000/api/updateOrder/'+id, data);
  }

  insertObject(temp){
   
    return this.httpClient.post('http://127.0.0.1:8000/api/insertObject/', temp);
  }

  deleteFromCart(id: string){
    return this.httpClient.delete('http://127.0.0.1:8000/api/deleteFromCart/'+id);
 
  }

  getNotifications(uid){
    return this.httpClient.get('http://127.0.0.1:8000/api/getNotifications/'+uid);
  }


  upadateQuantity(id: string,data: Cart){
    return this.httpClient.post('http://127.0.0.1:8000/api/increaseQuantity/'+id, data);
  }

  showCart(id: string){
    return this.httpClient.get('http://127.0.0.1:8000/api/showCart/'+id);
  }

  readOrNot(mid){
    return this.httpClient.get('http://127.0.0.1:8000/api/notificationReadOrNot/'+mid);
  }

  countMyNotification(uid){
    return this.httpClient.get('http://127.0.0.1:8000/api/countMyNotification/'+uid);
  }


  deleteOrderByAdmin(id: string){
    return this.httpClient.delete('http://127.0.0.1:8000/api/deleteOrderByAdmin/'+id);
  }


  getAdminAllData(){
    return this.httpClient.get('http://127.0.0.1:8000/api/getAdminAllData/');
  }

 
  
}
