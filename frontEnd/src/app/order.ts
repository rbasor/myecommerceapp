export class Order {
    product_id:any;
    user_id:any;
    status:any;
    payment_method:any;
    payment_status:any;
    id:any;
    name:any;
    price:any;
    image:any;
    address:any;
    address_id:any;
    specifications:any;
    category:any;
    quantity:any;
    mobile:any;
    state:any;
    zip:any;
    city:any;
}
