<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

use App\Http\Controllers\ProductController;
use App\Http\Controllers\AdminController;
//namespace App\Http\Controllers\API;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::post('login', [UserController::class,'login']);
Route::post('register', [UserController::class,'register']);

Route::post('loginAdmin', [AdminController::class,'loginAdmin']);
Route::post('registerAdmin', [AdminController::class,'registerAdmin']);

Route::get('getAllMembers',[AdminController::class,'getAll']); 

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('getProduct',[ProductController::class,'getProduct']); 

    Route::post('updateProduct/{id}', [ProductController::class,'updateProduct']);
    Route::post('imageUpload/{id}', [ProductController::class,'imageUpload']);
    Route::get('getProductById/{id}',[ProductController::class,'getProductById']);

    Route::get('getAllMembers',[AdminController::class,'getAll']); 
    Route::delete('deleteProduct/{id}', [ProductController::class,'deleteProduct']);

    Route::post('addProduct', [ProductController::class,'addProduct']);
    Route::post('insertObject', [AdminController::class,'insertProduct']);
    Route::post('updateUser/{id}', [UserController::class,'updateUser']);
    Route::post('addOrder', [UserController::class,'addOrder']);
    Route::post('addToCart', [UserController::class,'addToCart']);
    Route::post('addMessage', [UserController::class,'addMessage']);
    Route::get('getAllOrder',[UserController::class,'getAllOrder']); 
    Route::delete('deleteOrder/{id}', [UserController::class,'deleteOrder']);
    Route::delete('deleteFromCart/{id}', [UserController::class,'deleteFromCart']);
    Route::get('getOrderById/{id}',[UserController::class,'getOrderById']);
    Route::get('viewCart/{id}',[UserController::class,'viewCart']);
    Route::get('countCartItems/{id}',[UserController::class,'countCartItems']);

    Route::delete('deleteOrderByAdmin/{id}', [AdminController::class,'deleteOrderByAdmin']);


    Route::get('countMyNotification/{id}',[UserController::class,'countMyNotification']);
    Route::get('showCart/{id}',[UserController::class,'showCart']);
    Route::post('increaseQuantity/{id}', [UserController::class,'increaseQuantity']);
    Route::post('cartToSave/{id}',[UserController::class,'cartToSave']);


    Route::get('getAddressByUserId/{id}',[UserController::class,'getAddressByUserId']);
    Route::get('getNotifications/{id}',[UserController::class,'getNotifications']);
    Route::get('notificationReadOrNot/{id}',[UserController::class,'readOrNot']);
    Route::post('updateUserByAdmin/{id}', [AdminController::class,'updateUser']);
    Route::delete('deleteUser/{id}', [AdminController::class,'deleteUser']);
    Route::get('getAllMessages',[AdminController::class,'getAllMessages']); 
    Route::get('getOrder/{id}',[AdminController::class,'getOrder']);
    Route::get('getOrder/{id}',[AdminController::class,'getOrder']);
    Route::get('getAllUsers',[AdminController::class,'getAllUsers']); 
    Route::get('getAllCartItem',[AdminController::class,'getAllCartItem']);
    Route::get('getAllNotifications',[AdminController::class,'getAllNotifications']);

    Route::get('logout', [UserController::class,'logout']);
    
    Route::get('details', [UserController::class,'details']);


    Route::post('updateOrder/{id}', [AdminController::class,'updateOrder']);


    Route::get('getAdminAllData',[AdminController::class,'getAdminAllData']); 

});
