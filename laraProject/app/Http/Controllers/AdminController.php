<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\Cart; 
use App\Models\Location; 
use App\Models\Contact; 
use App\Models\Order; 
use App\Models\Notification; 
use App\Models\Product; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Carbon\Carbon;
class AdminController extends Controller
{

    public $successStatus = 200;


    public function insertProduct(Request $req){
      $product = New Product;
      $product->name = $req->name;
      $product->price = $req->price;
      $product->specifications = $req->specifications;
      $product->category = $req->category;
        $image = $req->file('image');
        $name = time().'.'.$image->getClientOriginalExtension();
        $product->image = $name;
        //$destinationPath = public_path('/images');
        //$image->move($destinationPath, $name);
        $image->move('uploads/products/',$name);


      if($product->save()){
          return response()->json([
              "message" => "Product record created"
          ], 201);
      }


    }




    public function getAll(){
      $user = User::get()->toJson(JSON_PRETTY_PRINT);
    return response($user, 200);
    }






    public function updateUser(Request $request, $id){
        if (User::where('id', $id)->exists()) {
            $user = User::find($id);
            $user->name = is_null($request->name) ? $user->name : $request->name;
            $user->password = is_null($request->password) ? $user->password : $request->password;
            $user->role = is_null($request->role) ? $user->role : $request->role;
            
            $user->save();
    
            return response()->json([
                "message" => "records updated successfully"
            ], 200);
            } else {
            return response()->json([
                "message" => "user not found"
            ], 404);
            
        }
    }




    public function deleteOrderByAdmin ($id) {
      if(Order::where('id', $id)->exists()) {
        $product = Order::find($id);
        $product->delete();

        return response()->json([
          "message" => "records deleted"
        ], 202);
      } else {
        return response()->json([
          "message" => "Order not found"
        ], 404);
      }
    }


    public function deleteUser ($id) {
        if(User::where('id', $id)->exists()) {
          $user = User::find($id);
          $user->delete();
  
          return response()->json([
            "message" => "records deleted"
          ], 202);
        } else {
          return response()->json([
            "message" => "User not found"
          ], 404);
        }
      }





      public function registerAdmin(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $input['role'] = 'admin';
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this-> successStatus); 
      }




      public function loginAdmin(Request $request)
    {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password'),'role' => request('role')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            //return response()->json(['success' => $success], $this-> successStatus); 
            return $success;
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }





    public function getAllUsers(){
       
        $user = User::get()->toJson(JSON_PRETTY_PRINT);
    return response($user, 200);
    }




    public function getAllNotifications(){
       
      $user = Notification::get()->toJson(JSON_PRETTY_PRINT);
      return response($user, 200);
  }





    public function getAllCartItem(){
      $cart = Cart::get()->toJson(JSON_PRETTY_PRINT);
      return response($cart, 200);
    }





    public function getAllAddress(){
       
      $location = Location::get()->toJson(JSON_PRETTY_PRINT);
      return response($location, 200);
  }





  public function getAllMessages(){
       
    $contact = Contact::get()->toJson(JSON_PRETTY_PRINT);
    return response($contact, 200);
}





public function updateOrder(Request $request, $id) {
  if (Order::where('id', $id)->exists()) {
      $order = Order::find($id);
      $order->status = is_null($request->status) ? $order->status : $request->status;
      $order->save();

      return response()->json([
          "message" => "records updated successfully"
      ], 200);
      } else {
      return response()->json([
          "message" => "Order not found"
      ], 404);
      
  }
}





public function getOrder($id){
  if (Order::where('id', $id)->exists()) {
      $order = Order::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
      return response($order, 200);
    } else {
      return response()->json([
        "message" => "user not found"
      ], 404);
    }
}



public function getAdminAllData(){
  $data          = array();
  $notifn = Notification::get();
  $notifnCount = count($notifn);

  $ordr = Order::get();
  $ordrCount = count($ordr);

  $prd = Product::get();
  $prdCount = count($prd);

  $usr = User::get();
  $usrCount = count($usr);




  $data[] = $ordrCount;
  $data[] = $prdCount;
  $data[] = $usrCount;
  $data[] = $notifnCount;

  return response()->json( [$data] );
  
}


}
