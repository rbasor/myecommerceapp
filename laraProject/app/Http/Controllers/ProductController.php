<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product; 
class ProductController extends Controller
{
    //

    

    public function imageUpload(Request $req, $id){

      if (Product::where('id', $id)->exists()) {
        $product = Product::find($id);
        if($req->hasfile('image')){
          $file = $req->file('image');
          $extension = $file->getClientOriginalExtension();
          $filename = time() . '.' . $extension;
          $file->move('uploads/products/',$filename);
          $product->image = $filename;
        }
          $product->save();
    
          return response()->json([
              "message" => "records updated successfully"
          ], 200);
          } else {
          return response()->json([
              "message" => "Product not found"
          ], 404);
          
      }
    }
  
    public function addProduct(Request $req){
        $product = New Product;
        $product->name = $req->name;
        $product->price = $req->price;
       $product->image = $req->image;
        $product->specifications = $req->specifications;
        $product->category = $req->category;

        
        if($req->hasfile('image')){
          $file = $req->file('image');
          $extension = $file->getClientOriginalExtension();
          $filename = time() . '.' . $extension;
          $file->move('uploads/products/',$filename);
          $product->image = $filename;
        }


        if($product->save()){
            return response()->json([
                "message" => "Product record created"
            ], 201);
        }
    }

    public function getProduct(){
       
        $products = Product::get()->toJson(JSON_PRETTY_PRINT);
    return response($products, 200);
    }

    public function getProductById($id){
        if (Product::where('id', $id)->exists()) {
            $product = Product::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($product, 200);
          } else {
            return response()->json([
              "message" => "Product not found"
            ], 404);
          }
    }


    public function deleteProduct ($id) {
        if(Product::where('id', $id)->exists()) {
          $product = Product::find($id);
          $product->delete();
  
          return response()->json([
            "message" => "records deleted"
          ], 202);
        } else {
          return response()->json([
            "message" => "Product not found"
          ], 404);
        }
      }


      public function updateProduct(Request $request, $id) {
        if (Product::where('id', $id)->exists()) {
            $product = Product::find($id);
            $product->name = is_null($request->name) ? $product->name : $request->name;
            $product->price = is_null($request->price) ? $product->price : $request->price;
            $product->image = is_null($request->image) ? $product->image : $request->image;
            $product->specifications = is_null($request->specifications) ? $product->specifications : $request->specifications;
            $product->save();
    
            return response()->json([
                "message" => "records updated successfully"
            ], 200);
            } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
            
        }
    }
}
