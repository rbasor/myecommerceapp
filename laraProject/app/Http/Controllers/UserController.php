<?php



namespace App\Http\Controllers;


use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use App\Models\Order;
use App\Models\Cart;
use App\Models\Location;
use App\Models\Contact;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Carbon\Carbon;

class UserController extends Controller
{
    //
    public $successStatus = 200;
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
            
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $input['role'] = "user";
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $request->name;
        return response()->json(['success'=>$success], $this-> successStatus); 
    }


    public function details() 
    { 
        $user = Auth::user(); 
        //console.log($user);
        return response()->json(['success' => $user], $this-> successStatus); 
    } 


    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            //$success['name'] = $user->name;
            //return response()->json(['success' => $success], $this-> successStatus); 
            return $success;
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function logout()
    {
      if (Auth::user()) {
        $user = Auth::user()->token();
        $user->revoke();

        return response()->json([
          'success' => true,
          'message' => 'Logout successfully'
      ]);
      }else {
        return response()->json([
          'success' => false,
          'message' => 'Unable to Logout'
        ]);
      }
     }

    public function updateUser(Request $request, $id){
        if (User::where('id', $id)->exists()) {
            $user = User::find($id);
            $user->name = is_null($request->name) ? $user->name : $request->name;
            $user->password = is_null($request->password) ? $user->password : $request->password;
            
            $user->save();
    
            return response()->json([
                "message" => "records updated successfully"
            ], 200);
            } else {
            return response()->json([
                "message" => "user not found"
            ], 404);
            
        }
    }


    public function addOrder(Request $req){
        $order = New Order;
        $order->product_id = $req->product_id;
        $order->user_id = $req->user_id;
       $order->status = 'pending';
       $order->image = $req->image;
       $order->quantity = $req->quantity;

       $order->price = $req->price;
        $order->payment_method = $req->payment_method;
        $order->payment_status = "success";
       $order->name = $req->name;

       
        $location = New Location();
        $location->user_id = $req->user_id;
        $location->address = $req->address;
        $location->save();
        $order->address_id = $location->id;
        $result = $order->save();

        $message = New Notification();
        $message->user_name = 'user';
        $message->order_id = $order->id;
        $message->user_id = $req->user_id;
        $message->order_status = 'pending';
        $message->readOrNot = 'unread';
        $message->product_name = $req->name;
        $message->save();
        if($result){

            return response()->json([
                "message" => "Order placed"
            ], 201);
        }
    }
    public function getAllOrder(){
       
        $orders = Order::get()->toJson(JSON_PRETTY_PRINT);
    return response($orders, 200);
    }


    public function getNotifications($id){
      if (Location::where('user_id', $id)->exists()) {
        $message = Notification::where('user_id', $id)->latest()->get()->toJson(JSON_PRETTY_PRINT);
        return response($message, 200);
      } else {
        return response()->json([
          "message" => "user not found"
        ], 404);
      }
    }


    public function getOrderById($id){
        if (Order::where('user_id', $id)->exists()) {
            $orders = Order::where('user_id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($orders, 200);
          } else {
            return response()->json([
              "message" => "user not found"
            ], 404);
          }
    }

    public function getAddressByUserId($id){
      if (Location::where('user_id', $id)->exists()) {
          $orders = Location::where('user_id', $id)->get()->toJson(JSON_PRETTY_PRINT);
          return response($orders, 200);
        } else {
          return response()->json([
            "message" => "user not found"
          ], 404);
        }
  }





    public function addToCart(Request $req){
        $cart = New Cart;
        $cart->product_id = $req->product_id;
        $cart->user_id = $req->user_id;
        $cart->price = $req->price;
        $cart->name = $req->name;
        $cart->quantity = $req->quantity;
        $cart->image = $req->image;
        if($cart->save()){
            return response()->json([
                "message" => "product added to cart"
            ], 201);
        }
    }


    public function addMessage(Request $req){
      $contact = New Contact;
      $contact->message = $req->message;
      $contact->subject = $req->subject;
      $contact->user_id = $req->user_id;
      $contact->email = $req->email;
      $contact->name = $req->name;
      if($contact->save()){
          return response()->json([
              "message" => "feedback posted to admin. Thank you!"
          ], 201);
      }
  }


  public function readOrNot($id){
    if (Notification::where('id', $id)->exists()) {
        $items = Notification::where('user_id', $id)->get()->toJson(JSON_PRETTY_PRINT);
        return response($items, 200);
        } 
      else {
        return response()->json([
            "message" => "notification not found"
        ], 404);
        
    }
}

    public function deleteOrder ($id) {
        if(Order::where('id', $id)->exists()) {
          $product = Order::find($id);
          $product->delete();
  
          return response()->json([
            "message" => "records deleted"
          ], 202);
        } else {
          return response()->json([
            "message" => "Order not found"
          ], 404);
        }
      }



      public function deleteFromCart ($id) {
        if(Cart::where('id', $id)->exists()) {
          $product = Cart::find($id);
          $product->delete();
  
          return response()->json([
            "message" => "records deleted"
          ], 202);
        } else {
          return response()->json([
            "message" => "Order not found"
          ], 404);
        }
      }





      public function viewCart($id){
        if (Location::where('user_id', $id)->exists()) {
            $cart = Cart::where('user_id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($cart, 200);
          } else {
            return response()->json([
              "message" => "user not found"
            ], 404);
          }
    }

    public function countCartItems($id){
      if (Location::where('user_id', $id)->exists()) {
        $cart = Cart::where('user_id', $id)->get();
        $cartCount = count($cart);
        return response($cartCount, 200);
      } else {
        return response()->json([
          "message" => "user not found"
        ], 404);
      }
    }



    public function countMyNotification($id){
      if (Location::where('user_id', $id)->exists()) {
        $cart = Notification::where('user_id', $id)->get();
        $cartCount = count($cart);
        return response($cartCount, 200);
      } else {
        return response()->json([
          "message" => "user not found"
        ], 404);
      }
    }

    public function showCart($id){
      if (Cart::where('id', $id)->exists()) {
          $cart = Cart::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
          return response($cart, 200);
        } else {
          return response()->json([
            "message" => "cart not found"
          ], 404);
        }
  }


    public function cartToSave(Request $req,$id){

      
      if (Cart::where('user_id', $id)->exists()) {
        $cart = Cart::where('user_id', $id)->get();
        
        foreach ($cart as $item) {
            $order = New Order;
            $order->product_id = $item->product_id;
            $order->user_id = $item->user_id;
            $order->status = 'pending';
            $order->payment_method = $req->payment_method;
            $order->image = $item->image;
            $order->payment_status = "success";
            $order->name = $item->name;
            $order->mobile = $req->mobile;
            $order->state = $req->state;
            $order->price = $item->price;
            $order->zip = $req->zip;
            $order->city = $req->city;
            $order->quantity = $item->quantity;

            $location = New Location();
            $location->user_id = $item->user_id;
            $location->address = $req->address;
            $location->save();
            $order->address_id = $location->id;
            $order->save();
          }

          $comment = Cart::where('user_id',$id)->delete();

          return response()->json([
            "message" => "All order placed "
          ], 200);

      } else {
        return response()->json([
          "message" => "user not found"
        ], 404);
      }
      
    }



    public function increaseQuantity(Request $request, $id){
      if (Cart::where('id', $id)->exists()) {
          $item = Cart::find($id);
          $item->quantity = is_null($request->quantity) ? $item->quantity : $request->quantity;
         
          $item->save();
  
          return response()->json([
              "message" => "records updated successfully"
          ], 200);
          } else {
          return response()->json([
              "message" => "user not found"
          ], 404);
          
      }
  }



}
